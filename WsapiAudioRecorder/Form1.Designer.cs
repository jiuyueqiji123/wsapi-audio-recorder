﻿namespace WsapiAudioRecorder
{
    partial class Form_Main
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要修改
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form_Main));
            this.btn_startrecord = new System.Windows.Forms.Button();
            this.btn_stoprecord = new System.Windows.Forms.Button();
            this.notifyIcon1 = new System.Windows.Forms.NotifyIcon(this.components);
            this.SuspendLayout();
            // 
            // btn_startrecord
            // 
            this.btn_startrecord.Location = new System.Drawing.Point(21, 22);
            this.btn_startrecord.Name = "btn_startrecord";
            this.btn_startrecord.Size = new System.Drawing.Size(84, 35);
            this.btn_startrecord.TabIndex = 0;
            this.btn_startrecord.Text = "开始录制";
            this.btn_startrecord.UseVisualStyleBackColor = true;
            this.btn_startrecord.Click += new System.EventHandler(this.OnStartRecordClick);
            // 
            // btn_stoprecord
            // 
            this.btn_stoprecord.Location = new System.Drawing.Point(130, 22);
            this.btn_stoprecord.Name = "btn_stoprecord";
            this.btn_stoprecord.Size = new System.Drawing.Size(84, 35);
            this.btn_stoprecord.TabIndex = 1;
            this.btn_stoprecord.Text = "停止录制";
            this.btn_stoprecord.UseVisualStyleBackColor = true;
            this.btn_stoprecord.Click += new System.EventHandler(this.OnStopRecordClick);
            // 
            // notifyIcon1
            // 
            this.notifyIcon1.BalloonTipText = "fffff";
            this.notifyIcon1.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon1.Icon")));
            this.notifyIcon1.Text = "recorder";
            this.notifyIcon1.Visible = true;
            this.notifyIcon1.MouseDoubleClick += new System.Windows.Forms.MouseEventHandler(this.notifyIcon1_MouseDoubleClick);
            // 
            // Form_Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(242, 70);
            this.Controls.Add(this.btn_stoprecord);
            this.Controls.Add(this.btn_startrecord);
            this.MaximizeBox = false;
            this.Name = "Form_Main";
            this.ShowIcon = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.Manual;
            this.Text = "音频录制器";
            this.SizeChanged += new System.EventHandler(this.Form1_SizeChanged);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button btn_startrecord;
        private System.Windows.Forms.Button btn_stoprecord;
        private System.Windows.Forms.NotifyIcon notifyIcon1;
    }
}

