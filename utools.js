function StringFormat() 
{
         if (arguments.length == 0)
             return null;
         var str = arguments[0];
         for (var i = 1; i < arguments.length; i++) 
         {
             var re = new RegExp('\\{' + (i - 1) + '\\}', 'gm');
             str = str.replace(re, arguments[i]);
         }
         return str;
}

//遍历文件夹
function mapDir(dir, data, finishedcallback) {
    fs.readdir(dir, function(err, files) {

        if (err) {
            console.error(err)
            return
        }

        files.forEach((filename, index) => {
            if (data['isfinded']) {
                return
            }
            let pathname = path.join(dir, filename)
            fs.stat(pathname, (err, stats) => { // 读取文件信息
                if (err) {
                    console.log('获取文件stats失败')
                    return
                }

                if (stats.isDirectory()) {
                    mapDir(pathname, data,finishedcallback)
                } else if (stats.isFile()) {
                    if (path.basename(pathname) == data['tofind']) {
                        console.log(pathname)
                        data['isfinded'] = true
                        data['result'] = pathname
                        finishedcallback && finishedcallback(data)
                    }

                }
            })
        })
    })
}

function get_filefullpath_from_baseDir(baseDir, filename, finishedcallback) {
    env_realpath = baseDir
    var data = {
        "isfinded": false,
        'tofind': '',
        'result': ''
    }
    data['tofind'] = filename

    regstr = '%.*%'
    reg = new RegExp(regstr)
    if (reg.test(env_realpath)) {
        mat = reg.exec(env_realpath)[0]
        matt = String(mat).substr(1, mat.length - 2)
        env_real = process.env[matt]
        env_realpath = baseDir.replace(mat, env_real)
    }
    mapDir(env_realpath, data, finishedcallback)
}

str = String.raw`{{input}}`
savefilename = '{{subinput:要保存的文件名}}'
if(savefilename == ""){
  savefilename = StringFormat('edge_text2audio_{0}',Date.now())
}

tempfile_path = 'C:/temp.txt'
fs.writeFileSync(tempfile_path, str)
try{
  get_filefullpath_from_baseDir('%PROGRAMFILES(X86)%/Microsoft', 'msedge.exe', (data) => {
    
    //启动录音机
    var recorddir = 'C:/MyRecord'
    if(!fs.existsSync(recorddir)){
      fs.mkdirSync(recorddir)
    }
    var recorderPath = process.env['UtoolsCoreAssets'] + '/recorder/WsapiAudioRecorder.exe'
    var r_cmd = StringFormat('"{0}" -p "{1}/{2}.mp3" -t -art -1 -acp',recorderPath,recorddir,savefilename)
    console.log(r_cmd)
    child_process.exec(r_cmd)
    quickcommand.sleep(700)
    
    edge_path = data['result']
    var cmd = StringFormat('"{0}" "{1}"', edge_path, tempfile_path)
    console.log(cmd)
    child_process.exec(cmd)
    quickcommand.sleep(800)
	//模拟按下edge浏览器自带朗读快捷键，开启朗读
    keyTap("u", "shift", "ctrl")
    
	quickcommand.sleep(300)
    utools.outPlugin()
})
}catch(e){
  console.log(e)
}
